<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <title>Dijkstra's Shortest Path Algorithm</title>

  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/reveal.css">
  <link rel="stylesheet" href="css/theme/night.css">
  <link rel="stylesheet" href="css/slides.css">

  <!-- Theme used for syntax highlighting of code -->
  <link rel="stylesheet" href="lib/css/monokai.css">

  <!-- Override styles -->
  <style>
    .reveal .slides {
        margin-top: 50px;
        text-align: left;
    }
    .reveal .slides > section.present:not(.stack),
    .reveal .slides > section.stack section.present {
        margin: 0px;
        padding: 20px;
        background-color: #2b2b2bA0;
    }
    .reveal code {
        font-family: "Hack Nerd Font Mono", monospace;
    }
    .reveal pre code {
        max-height: 800px;
        color: #ccccccff;
    }
    .reveal code {
        color: #DAA520FF;
    }
  </style>

  <!-- Printing and PDF exports -->
  <script>
    var link = document.createElement( 'link' );
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
    document.getElementsByTagName( 'head' )[0].appendChild( link );
  </script>
</head>
<body>
<div class="reveal">
  <div class="slides">
    <section data-markdown data-separator="\n---\n"
             data-separator-vertical="\n--\n"
             data-background-image="./images/CodestarBackground.png"
             data-background-size="100%">
      <script type="text/template">
        ## Dijkstra's Shortest Path Algorithm
        ### Explanation and practical application
        Brown bag by Jean-Marc van Leerdam

        All images created by Jean-Marc van Leerdam, unless stated otherwise
        <!-- .element: class="img-attribution" -->
        ---
        ### Dijkstra's Algorithm
        * Finds the shortest path between two nodes in a weighted graph if a path exists
        * Edge weights have to be positive (which also implies non-zero)

        ![Example Graph](./images/dijkstra/graphSample.png)<!-- .element: style="display: block; margin-left: auto; margin-right: auto; width: 50%" -->

        ---
        ### How does it work
        The algorithm can be summarized as follows:

        * Input is a graph of vertices and edges that have a (nonzero positive) cost to travel from one vertex to the other connected vertex
        * Keep a collection of all vertices and track the estimated cost to reach them
          * Give the starting vertex a cost of 0
          * Give all other vertices an estimated cost of infinity
        * Keep a collection of visited vertices (initially empty)

        --
        ### How does it work (2)
        * From the collection of all vertices, pick the one with the lowest cost to reach it, that has not yet been visited
        * Make this the current vertex:
          * Add the vertex to the collection of visited vertices
          * Determine all vertices that can be reached from the current vertex
          * If the cost to reach these new vertices is lower than their current cost estimate, update the cost estimate
        * Stop when the current vertex is the target vertex

        --
        ### How does it work (3)
        To find the shortest paths to all reachable vertices, continue the process until there are no more unvisited
        vertices with a cost less than infinity.

        If there are only unvisited vertices with a cost of infinity, this means they are unreachable from the starting
        vertex.

        ---
        ### Example: Advent of Code 2021, Day 15
        https://adventofcode.com/2021/day/15

        The goal is to find the cheapest route through a grid, from top left to bottom right.

        Traversal is only allowed in horizontal or vertical steps (left, right, up, down), not diagonally

        ![Grid](./images/dijkstra/aoc2021_15_grid.png)
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        ![Grid](./images/dijkstra/aoc2021_15_path1.png)<!-- .element: class="fragment" -->
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        ![Grid](./images/dijkstra/aoc2021_15_path.png)<!-- .element: class="fragment" -->

        --
        ### Grid as a graph
        The grid can be interpreted as a graph, with each cell being a vertex that is connected to its
        immediate neighbors. The cell value is used as the cost to reach that cell.

        Instead of storing the graph itself, a `getNeigbors` method can be written that provides the collection
        of cells (vertices) that can be reached from a cell.

        The grid can only be traversed horizontally or vertically, so there are at most 4 neighbors
        ```scala
        val getNeighbors = (x: Int, y: Int) =>
          List(
            (x, y + 1),
            (x, y - 1),
            (x - 1, y),
            (x + 1, y)
          ).filter(e => e._1 >= 0 && e._1 < maxX && e._2 >= 0 && e._2 < maxY)
        ```
        --
        ### Initialise the data structures
        ```scala
        // The grid
        val plan: Array[Array[Int]] = ???

        // Boolean array to track which vertices have been visited
        val visited = Array.ofDim[Boolean](plan.length, plan(0).length)

        // priority queue of reachable vertices (lowest distance first)
        val reachableVertices = mutable.PriorityQueue[(Int, (Int, Int))]().reverse

        // add the starting vertex, which is reachable at cost zero
        reachableVertices.enqueue((0, (0, 0)))

        ```
        --
        ### Loop until we reach the target node
        ```scala
        var done = false
        while (reachableVertices.nonEmpty && !done) {
          val (distance, vertex) = reachableVertices.dequeue()
          // skip if vertex has already been visited (if it is, then that was via a shorter path)
          if (!visited(vertex._2)(vertex._1)) {
            process(vertex, distance)
          }
          if (vertex._1 == maxX - 1 && vertex._2 == maxY - 1) {
            done = true
            println(s"Found destination at cost $distance")
          }
        }
        ```
        --
        ### The process step
        Processing a vertex means determining which vertices can be reached, and enqueuing them with the cost value
        when reaching them from the current vertex

        We only have to consider neighbors that have not yet been visited
        ```scala
        def process(vertex: (Int, Int), distance: Int): Unit = {
          val possibles = getNeighbors.tupled(vertex).filter(n => !visited(n._2)(n._1))
          possibles.foreach(next => {
            val riskLevel = distance + plan(next._2)(next._1)
            reachableVertices.enqueue((riskLevel, next))
          })
          visited(vertex._2)(vertex._1) = true
        }
        ```
        --
        ### Day 15 demo + questions

        * https://adventofcode.com/2021/day/15
        * https://jeanmarc-nl.bitbucket.io/aoc2021/2021/day/15/

        ---
        ### Example: Advent of Code 2021, Day 23
        https://adventofcode.com/2021/day/23

        The goal is to find the cheapest set of moves to move pieces on a board into their final position.

        There are 4 types of pieces (A, B, C, and D), and the cost to move each piece depends on the type:
        <ul>
          <li>Type A cost per step 1</li>
          <li>Type B cost per step 10</li>
          <li>Type C cost per step 100</li>
          <li>Type D cost per step 1000</li>
        </ul>

        The board consists of a hallway with 7 possible positions and 4 rooms that each can hold 4 pieces. Each room
        is the home for one of the types.
        --
        The starting position has all 16 pieces in the rooms, but not necessarily in the correct room.

        Through a series of individual moves, all pieces have to be put in the correct room.

        Pieces cannot pass each other, so their path must be unobstructed. Pieces are not allowed to stop directly
        in front of a room (the spaces shown in the diagram have to remain empty at all times)

        ![Board](./images/dijkstra/Amphipods.png)<!-- .element: style="display: block; margin-left: auto; margin-right: auto; width: 50%" -->

        --
        ### Board rules
        Pieces are only allowed to move in 2 ways:

        * From a room into the hallway
        * From the hallway into their own room

        The board puzzle can be solved by translating the problem statement in such a way that solving for the shortest
        path gives us the solution to the problem.

        In this case we can treat board positions as vertices, and each possible move as the edges leading from a
        board position to other board positions. The cost of each move can be used as the weight. This results in a
        (very) big directed graph to which we can apply Dijkstra's algorithm.

        --
        ### From board moves to graph
        Although the entire graph would be way too big to materialize, we can still traverse it by starting with the
        initial board, and working towards the known end state while we keep track of the relevant boards + moves.

        Optimisations:
        * We can discard any known board position that we have already produced in a cheaper way
        * We can discard any board position that has a greater cost than any already known solution (because it can
        never lead to a better solution)

        --
        ### Memory consumption considerations
        Since we will be considering a large amount of possible boards, the memory efficiency is somewhat important.

        The board has 23 positions (7 hallway spots + 4 * 4 room spots), so we can represent each board position as a
        23 character string of A's, B's, C's, D's and spaces.

        ![Board](./images/dijkstra/Samples.png)<!-- .element: style="display: block; margin-left: auto; margin-right: auto; width: 80%" -->
        --
        ### Setup the data structures
        ```scala
        val startPos = BurrowBoard.fromInput(lines)

        // a map of known Boards, with the score + moves to reach it
        val knownBoards = mutable.Map[String, (Int, List[Move])]()

        // push the start position as a known board
        knownBoards(startPos) = (0, List.empty)

        val boardQueue = mutable.PriorityQueue[(Int, String)]().reverse

        // add the start position to the queue
        boardQueue.enqueue((0, startPos))

        var counter = 0
        var lowestFinish = Int.MaxValue // known upper bound for the score

        ```
        --
        ### The main loop
        ```scala
        while (boardQueue.nonEmpty) {
          counter += 1
          val (score, board) = boardQueue.dequeue()
          if (BurrowBoard.isFinish(board)) {
            println(s"We have a finish at ${score}")
            knownBoards(board)._2.foreach(println)
            lowestFinish = score
          }
          // if current score is the best known score, we consider all possible moves
          if (knownBoards(board)._1 == score) {
            val journey = knownBoards(board)._2
            val moves = BurrowBoard.findPossibleMoves(board)
            moves.foreach(m => {
              if (m.cost + score < lowestFinish) {
                val newBoard = BurrowBoard.move(board, m)
                // keep the new board if it is cheaper than the current known board (if any)
                if (!knownBoards.contains(newBoard) || knownBoards(newBoard)._1 > m.cost + score) {
                  knownBoards(newBoard) = (m.cost + score, journey :+ m)
                  boardQueue.enqueue((m.cost + score, newBoard))
                }
              }
            })
          }
        }
        ```
        --
        ### Determining valid moves
        The challenge is to come up with a set of valid moves for a given board.

        I decided to write out all possibilities, but there are probably smarter ways to determine the set of legal
        moves.

        First I determine the available free spots (split in 2 groups: room spots and hallway spots)
        ```scala
        val (inFree, outFree) = freeSpots(board).partition(_ < 16)
        ```
        Then two groups of pieces that can be moved
        ```scala
        val (inCandidates, outCandidates) = movableAmphipods(board).partition(_ < 16)
        ```
        --
        ### Determining valid moves (cont'd)
        The candidates are filtered by removing pieces that are already in their final spot
        ```scala
        // in to out: only if not yet at the correct position with spots below it filled correctly too
        val movableInCandidates = inCandidates
          .map {
            case 0  => (0, board(0) != 'A')
            case 1  => (1, board(1) != 'A' || board(0) != 'A')
            case 2  => (2, board(2) != 'A' || board(1) != 'A' || board(0) != 'A')
            case 3  => (3, board(3) != 'A' || board(2) != 'A' || board(1) != 'A' || board(0) != 'A')
            case 4  => (4, board(4) != 'B')
            case 5  => (5, board(5) != 'B' || board(4) != 'B')
            case 6  => (6, board(6) != 'B' || board(5) != 'B' || board(4) != 'B')
            case 7  => (7, board(7) != 'B' || board(6) != 'B' || board(5) != 'B' || board(4) != 'B')
            case 8  => (8, board(8) != 'C')
            case 9  => (9, board(9) != 'C' || board(8) != 'C')
            case 10 => (10, board(10) != 'C' || board(9) != 'C' || board(8) != 'C')
            case 11 => (11, board(11) != 'C' || board(10) != 'C' || board(9) != 'C' || board(8) != 'C')
            case 12 => (12, board(12) != 'D')
            case 13 => (13, board(13) != 'D' || board(12) != 'D')
            case 14 => (14, board(14) != 'D' || board(13) != 'D' || board(12) != 'D')
            case 15 => (15, board(15) != 'D' || board(14) != 'D' || board(13) != 'D' || board(12) != 'D')
            case p  => (p, false)
          }
          .filter(_._2)
          .map(_._1)
        ```
        --
        ### Determining valid moves (cont'd)
        The in-spots are filtered by removing spots that are above empty spots or spots with wrong pieces
        ```scala
        val availableInSpots = inFree
          .map {
            case 0  => (0, true)
            case 1  => (1, board(0) == 'A')
            case 2  => (2, board(0) == 'A' && board(1) == 'A')
            case 3  => (3, board(0) == 'A' && board(1) == 'A' && board(2) == 'A')
            case 4  => (4, true)
            case 5  => (5, board(4) == 'B')
            case 6  => (6, board(4) == 'B' && board(5) == 'B')
            case 7  => (7, board(4) == 'B' && board(5) == 'B' && board(6) == 'B')
            case 8  => (8, true)
            case 9  => (9, board(8) == 'C')
            case 10 => (10, board(8) == 'C' && board(9) == 'C')
            case 11 => (11, board(8) == 'C' && board(9) == 'C' && board(10) == 'C')
            case 12 => (12, true)
            case 13 => (13, board(12) == 'D')
            case 14 => (14, board(12) == 'D' && board(13) == 'D')
            case 15 => (15, board(12) == 'D' && board(13) == 'D' && board(14) == 'D')
            case p  => (p, false)
          }
          .filter(_._2)
          .map(_._1)
        ```
        --
        ### Determining valid moves (cont'd)
        Now we have possible moves, all that remains is ensuring there are no obstructions
        ```scala
        val inMoves = (for {
          from <- outCandidates
          to <- availableInSpots
        } yield (from, to))
          .filter(p => canReach(p._1, p._2, board))

        val outMoves = (for {
          from <- movableInCandidates
          to <- outFree
        } yield (from, to))
          .filter(p => canReach(p._1, p._2, board))

        (inMoves ++ outMoves).map(m => Move(m._1, m._2, Amphipod.fromChar(board(m._1))))
        ```
        --
        ### Can reach explained
        The `canReach` method is another huge match statement that validates for each possible move if
        1. there are no obstructions (== all intermediate spots are empty)
        2. the piece type is allowed to enter the room
        3. the pieces below the target spot are also of the correct type
        --
        ### Determine the cost of a move
        A cost function determines the cost of moving a creature from one board position to another.

        I wrote another big match statement that determines the number of steps between all valid `to` and `from` positions,
        and then multiply that with the cost per step for the piece type.
        --
        ### Day 23 Demo & questions

        * https://adventofcode.com/2021/day/23
        * https://jeanmarc-nl.bitbucket.io/aoc2021/2021/day/23/

        ---
        ## Further improvements
        * Dijkstra's algorithm is guaranteed to work, but it is relatively naive
        * Optimisations are possible
          * Heuristics
            * Choose which edge(s) to process first based on an expected outcome
            * Use upper or lower bounds for the estimated journey to prefer or disqualify nodes
          * Knowledge
            * Precompute graph to determine waypoints that will be crossed when traversing from one area into another area
          * Bidirectional search
        --
        ## Bidirectional Dijkstra
        * Can be used if graph is bidirectional with equal weights in both directions
        * Search time could at most be halved (and will not take longer)
        * Start in parallel from both ends, and choose the next edge to process from both sets of vertices
        * Stopping conditions are not trivial
          * The first `match` of a vertex that is in both sets does not have to be the shortest path
        --
        ## Related algorithms
        * A* (A-star)
          * Heuristics based, with an `h(v)` that provides a lower bound estimate of the remaining cost to reach the goal
          * Identical to Dijkstra when using `h(v) = 0`
          * For graphs representing physical networks the straight-line distance can be used
          * In grids (like game boards), the Manhattan distance can be used
        * Bidirectional A*
          * Not quite as easy to apply
          * There are additional constraints to the heuristics functions to guarantee that Bidirectional A* produces the shortest path
          * Similar challenges in determining if shortest path has been found
      </script>
    </section>
    <section data-background-image="./images/CodestarBackground.png"
             data-background-size="100%">
      <h4>Thank you for your attention</h4>
      <ul>
        <li>Any remarks or questions?</li>
      </ul>
    </section>
  </div>
</div>

<script src="js/reveal.js"></script>

<script>
  // More info about config & dependencies:
  // - https://github.com/hakimel/reveal.js#configuration
  // - https://github.com/hakimel/reveal.js#dependencies
  Reveal.initialize({
    dependencies: [
      { src: 'plugin/markdown/marked.js' },
      { src: 'plugin/markdown/markdown.js' },
      { src: 'plugin/notes/notes.js', async: true },
      { src: 'plugin/highlight/highlight.js', async: true }
    ],
    width: 1600,
    height: 1200,
    center: false
  });
</script>
</body>
</html>
