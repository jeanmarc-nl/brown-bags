<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Domain Driven Design - an introduction</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/reveal.css">
    <link rel="stylesheet" href="css/theme/night.css">
    <link rel="stylesheet" href="css/slides.css">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="lib/css/monokai.css">

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
  </head>
  <body>
    <div class="reveal">
      <div class="slides">
        <section data-markdown data-separator="\n---\n" data-separator-vertical="\n--\n">
          <script type="text/template">
            ## Domain Driven Design
            ### 3 day workshop summary
            Workshop by Nick Tune, February 2020

            Summary by Jean-Marc van Leerdam

            All images taken from slide deck provided by Nick Tune, unless stated otherwise.
            <!-- .element: class="img-attribution" -->
            ---
            ### Workshop objectives
            * Understand Domain Driven Design philosophy
            * Appreciate usefulness of principles
            * Hands on experience with techniques

            Note:
            Full slide deck is available on request, for Ordina employees

            ---
            ### Schedule
            ![DDD Schedule](./images/ddd/DDDschedule.png)
            <!-- .element: style="padding-left: 10%;padding-right: 10%;" -->

            My presentation covers day 1 and 2

            ---
            ### Goals in System Design
            * Reduce dependencies
            * Keep codebases small
            * Focus on high internal quality

            ![Quality is Speed](./images/ddd/QualityIsSpeed.png)
            <!-- .element: style="padding-left: 20%;padding-right: 20%;" -->
            --
            ### Goals in System Design (2)
            * Essential Complexity vs. Accidental Complexity

            ![Essential vs. Accidental](./images/ddd/EssentialAccidental.png)
            <!-- .element: style="padding-left: 20%;padding-right: 20%;" -->

            Note:
            Examples are
            * Over engineered code
            * Bad naming
            * Bad code structures
            * Boundaries set for historical / political reasons
            * Insuffcient encapsulation

            --
            ### Goals in System Design (3)
            * Key success factors
                * Loosely coupled software architecture
                * Organisation structure to match it
            * Contain complexity within components and (thus) teams
            * Enables Continuous Delivery Performance
            * Enables efficient scaling of organisation and performance
            --
            ### Goals in System Design (4)
            * Current IT Requirements demand large scale solutions
                * IoT, Always Connected (mobile, wearables), Integrated Solutions
            * Time to Market expected in weeks, not years
            * Evolving your systems is the new reality
            ---
            ### Domain Driven Design
            * Provide a way to assess trade-offs from multiple perspectives

            ![Perspectives](./images/ddd/Perspectives.png)
            <!-- .element: style="padding-left: 20%;padding-right: 20%;" -->
            --
            ### Domain Driven Design (2)
            * Assist in achieving 3 goals

            ![DDD Goals](./images/ddd/DDDgoals.png)
            <!-- .element: style="padding-left: 20%;padding-right: 20%;" -->

            --
            ### Domain Driven Design (3)
            * Iterative process
            ![Phases](./images/ddd/Phases.png)

            ---
            ### Domain Discovery
            * Deep understanding of the domain allows better fit of your design
            * Models are by definition imperfect and contain trade-offs
            * Accidental complexity can only be avoided by knowing the domain
            * Business Model determines what the trade-offs should be

            ---
            ### Discovery Techniques
            * Business Model Canvas
            * Event Storming
            * Domain Storytelling
            * Story Storming
            * Example Mapping

            Combine techniques to eliminate blind spots that each technique has

            --
            ### Business Model Canvas
            ![Business Canvas](./images/ddd/BusinessCanvas.png)
            <!-- .element: style="padding-left: 10%;padding-right: 10%;" -->

            --
            ### Event Storming
            * Write down many detailed events
            * Consider specific events, not generalisations
            * Use business terms to describe the event
            * Challenge the events, find the edge cases
            * Detect 'pivotal' events that separate distinct groups of events

            Tips: https://medium.com/nick-tune-tech-strategy-blog/eventstorming-modelling-tips-to-facilitate-microservice-design-1b1b0b838efc
            <!-- .element: class="img-attribution" -->

            --
            ### Domain Storytelling
            ![Domain Storytelling](./images/ddd/DomainStoryTelling.png)

            domainstorytelling.org
            <!-- .element: class="img-attribution" -->

            --
            ### Story Storming
            * Using color coded post-its, come up with use cases
            * Subject (Y), Verb (B), Object (G), System (R), Alternative (P)

            ![Story Storming](./images/ddd/StoryStorming.png)
            <!-- .element: style="padding-left: 30%;padding-right: 30%;" -->

            --
            ### Example Mapping
            * Add details and edge cases to story steps
            * Gain insight into relative importance
            * Discover unexpected similarities

            ---
            ### Decompose, Integrate
            * Divide and conquer has proven to be successful
            * But each divide adds complexity
            * The challenge: find the optimal way to divide

            The latest approach to Divide & Conquer:
            * Bounded Contexts

            --
            ### Bounded Contexts (1)
            ![Expect Change](./images/ddd/ExpectChange.png)
            <!-- .element: style="padding-left: 20%;padding-right: 20%;" -->

            --
            ### Bounded Contexts (2)
            * Ubiquitous Language
            * Well defined, unambiguous, understood by all stakeholders
            * Valid within a clearly defined area: the bounded context (BC)
            * Terms can have different meaning across BCs
            * Between BCs, translations are needed to communicate

            --
            ### Bounded Contexts (3)
            * Within a BC
                * tight coupling is used to build efficient solutions
            * Between BCs
                * loose coupling is maintained to limit change impact
            * No need for shared code, shared database, nor canonical data model
            * Align your teams to the BCs (domain)
            * Reduces cross team dependencies

            --
            ### Bounded Contexts (4)
            * Describe BC using the Bounded Context Canvas

            ![Bounded Context Canvas](./images/ddd/BoundedContextCanvas.png)
            <!-- .element: style="padding-left: 20%;padding-right: 20%;" -->


            --
            ### Bounded Contexts (5)
            * Sorting / grouping the events/stories/examples

            ![Sorting Strategies](./images/ddd/SortingStrategies.png)
            <!-- .element: style="padding-left: 20%;padding-right: 20%;" -->

            ---
            ### Communication
            * Static view only tells half the story
            * Interactions are essential but complex
            * Mutability, race conditions, deadlocks, eventual consistency

            --
            ### Communication (2)
            DDD solution to reducing impact:
            * Communicate between BCs via Domain Events
            * Three types:
                * Commands - fire & forget to a single recipient
                * Events - fire & forget to multiple recipients
                * Queries - request to a single source, expecting a single reply

            --
            ### Domain Events (1)
            * Prefer Domain Events over State Change Events
                * DE conveys meaning
                * SC forces consumer to interpret/derive meaning
            * Balance Fat events versus Lean events
                * Fat events increase coupling
                * Lean events require caching

            --
            ### Domain Events (2)
            * Minimise public events
                * Reduces coupling
            * Consider consolidating events
                * Summary events
                * Smoothed events
                * Delta Events (include current + previous)
                * Ephemeral Events (only latest event is relevant)

            ---
            ### Strategic Classification
            * Determine position and direction of BCs

            ![Strategic Classification](./images/ddd/StrategicClassification.png)
            <!-- .element: style="padding-left: 20%;padding-right: 20%;" -->

            --
            ### Context Qualification

            ![Context Qualification](./images/ddd/ContextQualification.png)
            <!-- .element: style="padding-left: 10%;padding-right: 10%;" -->

            --
            ### Context Qualification
            * Put your effort in areas where it matters: Core
            * Choose existing solutions for 'Generic' or 'Supporting'
            * Anticipate expected drift

            ![Context Qualification](./images/ddd/ContextQualification.png)
            <!-- .element: style="padding-left: 40%;padding-right: 40%;" -->

            --
            ### Example Drifts - Decisive Core
            ![Decisive Core](./images/ddd/DecisiveCore.png)

            --
            ### Example Drifts - Hidden Core
            ![Hidden Core](./images/ddd/HiddenCore.png)

            --
            ### Example Drifts - First To Market
            ![First to Market Core](./images/ddd/FirstToMarketCore.png)

            --
            ### Example Drifts - Suspect Supporting
            ![Suspect Supporting](./images/ddd/SuspectSupporting.png)

            --
            ### Example Drifts - Black Swan
            ![Black Swan](./images/ddd/BlackSwan.png)

          </script>
        </section>
        <section>
          <h4>Questions?</h4>
          <ul>
            <li>...</li>
          </ul>
        </section>
      </div>
    </div>

    <script src="js/reveal.js"></script>

    <script>
      // More info about config & dependencies:
      // - https://github.com/hakimel/reveal.js#configuration
      // - https://github.com/hakimel/reveal.js#dependencies
      Reveal.initialize({
        dependencies: [
          { src: 'plugin/markdown/marked.js' },
          { src: 'plugin/markdown/markdown.js' },
          { src: 'plugin/notes/notes.js', async: true },
          { src: 'plugin/highlight/highlight.js', async: true }
        ]
      });
    </script>
  </body>
</html>
